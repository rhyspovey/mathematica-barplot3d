(* ::Package:: *)

(* ::Title:: *)
(*BarPlot3D*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["BarPlot3D`"];


BarPlot3D::usage="BarPlot3D[] maskes a 3D bar plot. \"Size\" is divided by BoxRatios to get scaled dimensions.";


BarPlot3D::depth="Data depth imcompatible.";


BarPlot3D::plotrange="PlotRange not recognized.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


Deepen[\[FormalX]_,\[FormalN]_Integer]:=Nest[List,\[FormalX],\[FormalN]-Depth[\[FormalX]]];


Cuboidize[scale_,offset_][data_/;Depth[data]==3]:=Module[{n=Dimensions[data][[2]]},
Switch[n,
	3,Cuboid[ImageScaled[Append[-scale+offset,0],Append[#[[;;2]],0]],ImageScaled[Append[scale+offset,0],#]]&/@data,
	4,Cuboid[ImageScaled[Append[-scale+offset,0],Part[#,{1,2,3}]],ImageScaled[Append[scale+offset,0],Part[#,{1,2,4}]]]&/@data
]
];


BarPlot3DOptions={
Lighting->"Neutral",
FaceGrids->{{0,0,-1}},
BoxRatios->{1,1,0.4},
PlotRangePadding->{Scaled[0.1],Scaled[0.1],0},
ViewProjection->"Orthographic",
PlotStyle->ColorData[97,"ColorList"],
"Size"->0.05,
"Offset"->{0,0},
"PlaneStyle"->Automatic
};


Options[BarPlot3D]=Join[FilterRules[Options[ListPointPlot3D],Except[Alternatives@@Keys[BarPlot3DOptions]]],BarPlot3DOptions];


BarPlot3D[data_,opts:OptionsPattern[]]:=Module[{
n,listpointopts,
size=OptionValue["Size"],boxratios=OptionValue[BoxRatios][[1;;2]],scales,
offsets,plotstyles,plotrange,autorange,basestyle,autostyle
},

(* work with list of datas *)
n=Length[Deepen[data,4]];

(* plotstyles *)
plotstyles=If[ListQ@OptionValue[PlotStyle],
	PadRight[OptionValue[PlotStyle],n,OptionValue[PlotStyle]],
	ConstantArray[OptionValue[PlotStyle],n]
];

(* scales *)
scales=Switch[Depth[size],
	1,ConstantArray[{size,size}/boxratios,n],
	2,If[n==1,{size/boxratios},({#,#}/boxratios)&/@PadRight[size,n,size]],
	3,#/boxratios&/@PadRight[size,n,size]
];

(* offsets *)
offsets=With[{offset=Deepen[OptionValue["Offset"],3]},PadRight[offset,n,offset]];

(* plotrange *)
autorange=Switch[Dimensions[First[Deepen[data,4]]][[2]],
	(*3,{0,Max[Deepen[data,4][[All,All,-1]]]},*)
	3,{0,Full},
	4,Automatic
];
plotrange=If[ListQ[OptionValue[PlotRange]],
	Which[
		(Length[OptionValue[PlotRange]]==2)\[And](Depth[OptionValue[PlotRange]]==2),{Full,Full,OptionValue[PlotRange]},
		OptionValue[PlotRange][[3]]===Automatic,Append[OptionValue[PlotRange][[;;2]],autorange],
		True,OptionValue[PlotRange]
	],
	Which[
		OptionValue[PlotRange]===Automatic,{Full,Full,autorange},
		NumberQ[OptionValue[PlotRange]],{Full,Full,{0,OptionValue[PlotRange]}},
		True,Message[BarPlot3D::plotrange]
	]
];

(* pass options through ListPointPlot3D *)
listpointopts=AbsoluteOptions[
	ListPointPlot3D[data,Join[
		FilterRules[(#->OptionValue[#])&/@DeleteCases[Keys[Options[BarPlot3D]],PlotRange],Options[ListPointPlot3D]],
		{PlotRange->plotrange}
		]
	]];

(* basestyle *)
autostyle=Switch[Dimensions[First[Deepen[data,4]]][[2]],
	3,{0,Directive[Gray,Opacity[0.5]]},
	4,None
];
basestyle=Switch[OptionValue["PlaneStyle"],
	None,None,
	Automatic,autostyle,
	_?NumberQ,{OptionValue["PlaneStyle"],Directive[Gray,Opacity[0.5]]},
	Directive[__],{0,OptionValue["PlaneStyle"]},
	{_?NumberQ,Directive[__]},OptionValue["PlaneStyle"],
	{_?NumberQ,__},{OptionValue["PlaneStyle"][[1]],Directive@@(OptionValue["PlaneStyle"][[2;;]])},
	_List,{0,Directive[OptionValue["PlaneStyle"]]}
];

Graphics3D[
	Join[
		If[ListQ@basestyle,{basestyle[[2]],InfinitePlane[{0,0,basestyle[[1]]},{{1,0,0},{0,1,0}}],Opacity[1]},{}],
		MapThread[{#2}~Join~Cuboidize[#3,#4][#1]&,{Deepen[data,4],plotstyles,scales,offsets}]
	],
	Join[
		FilterRules[(#->OptionValue[#])&/@DeleteCases[Keys[Options[BarPlot3D]],PlotRange|Ticks|FaceGrids],Options[ListPointPlot3D]],
		FilterRules[listpointopts,{PlotRange,Ticks,FaceGrids}]
	]
]

]/;If[MemberQ[{3,4},Depth[data]],True,Message[BarPlot3D::depth];False]


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
